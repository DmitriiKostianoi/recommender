package recommender.domain

sealed trait Gender
object Gender {
  case object Female extends Gender
  case object Male extends Gender
}

case class Email(value: String) extends AnyVal

case class UserInfo(userName: String, email: Email, age: Age, gender: Gender)

sealed trait Action
object Action {
  case object Like extends Action
  case object Skip extends Action
  case object Play extends Action
}

case class UserId(value: Long) extends AnyVal {
  override def toString = value.toString
}
case class VideoId(value: Long) extends AnyVal {
  override def toString = value.toString
}

case class Recommendation(userId: UserId, videoId: VideoId)
case class UserAction(userId: UserId, videoId: VideoId, action: Action)
