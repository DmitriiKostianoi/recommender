package recommender.api

object Requests {
  case class RegisterRequest(userName: String, email: String, age: Int, gender: Int)
  case class ActionRequest(userId: Long, videoId: Long, actionId: Int)
}
