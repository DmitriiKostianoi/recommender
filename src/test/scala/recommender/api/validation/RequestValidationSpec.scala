package recommender.api.validation

import cats.data.NonEmptyList
import cats.data.Validated.{Invalid, Valid}
import org.specs2.mutable.Specification
import recommender.api.Requests.{ActionRequest, RegisterRequest}
import ValidationError._
import recommender.domain._

class RequestValidationSpec extends Specification {
  import RequestValidation._

  "Request validation" >> {
    "accumulates errors for a register request" >> {
      val request = RegisterRequest(
        userName = "SomeUser",
        email = "not a valid email",
        age = 1234,
        gender = 0
      )

      validate(request) mustEqual
        Invalid(NonEmptyList.of(EmailIsInvalid, AgeIsInvalid, GenderIsInvalid))
    }

    "converts a valid register request to a user info" >> {
      val request = RegisterRequest(
        userName = "SomeUser",
        email = "user@user.com",
        age = 23,
        gender = 1
      )

      validate(request) mustEqual
        Valid(UserInfo("SomeUser", Email("user@user.com"), 23, Gender.Male))
    }

    "accumulates errors for an action request" >> {
      val request = ActionRequest(
        userId = 123L,
        videoId = 123L,
        actionId = 5
      )

      validate(request) mustEqual
        Invalid(NonEmptyList.of(ActionIsInvalid))
    }

    "converts a valid action request to a user action" >> {
      val request = ActionRequest(
        userId = 123L,
        videoId = 321L,
        actionId = 1
      )

      validate(request) mustEqual
        Valid(UserAction(UserId(123L), VideoId(321L), Action.Like))
    }
  }
}
