package recommender.api

import scala.concurrent.Future

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.Specs2RouteTest
import akka.http.scaladsl.server.Route._
import org.specs2.mutable.Specification
import recommender.domain.Recommender.Result
import recommender.domain.Recommender.Error._
import recommender.domain._
import cats.syntax.either._
import org.specs2.specification.Scope
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport._
import io.circe.Json
import io.circe.parser._
import io.circe.generic.auto._

class HttpApiSpec extends Specification with Specs2RouteTest {

  val recommendationResponse =
    """
      |{
      |  "userId": 1,
      |  "videoId": 1
      |}
    """.stripMargin

  "HTTP API" >> {
    "handles register requests" >> {
      val validRequest =
        """
          |{
          |  "userName": "user",
          |  "email": "user@user.com",
          |  "age": 23,
          |  "gender": 1
          |}
        """.stripMargin

      "returning a recommendation if registration was successful" >> new TestScope {
        Post("/register", parse(validRequest)) ~>
          route() ~>
          check {
            status mustEqual StatusCodes.OK
            responseAs[Json] mustEqual parse(recommendationResponse).right.get
          }
      }

      "returning an error if the request was invalid" >> new TestScope {
        val request =
          """
            |{
            |  "userName": "user",
            |  "email": "not an email",
            |  "age": 1,
            |  "gender": 5
            |}
          """.stripMargin

        val expectedResponse =
          """
            |{
            |  "errors": ["email is not valid", "age is not valid", "gender is not valid"]
            |}
          """.stripMargin

        Post("/register", parse(request)) ~>
          route() ~>
          check {
            status mustEqual StatusCodes.BadRequest
            responseAs[Json] mustEqual parse(expectedResponse).right.get
          }
      }

      "returning an error if registration failed" >> new TestScope {
        val expectedResponse =
          """
            |{
            |  "errors": ["userId does not exist"]
            |}
          """.stripMargin

        Post("/register", parse(validRequest)) ~>
          route(failingRecommender) ~>
          check {
            status mustEqual StatusCodes.BadRequest
            responseAs[Json] mustEqual parse(expectedResponse).right.get
          }
      }
    }

    "handle action requests" >> {
      val validRequest =
        """
          |{
          |  "userId": 1,
          |  "videoId": 1,
          |  "actionId": 1
          |}
        """.stripMargin

      "returning a new recommendation in case of a success" >> new TestScope {
        Post("/action", parse(validRequest)) ~>
          route() ~>
          check {
            status mustEqual StatusCodes.OK
            responseAs[Json] mustEqual parse(recommendationResponse).right.get
          }
      }

      "returning an error if the request was invalid" >> new TestScope {
        val request =
          """
            |{
            |  "userId": 1,
            |  "videoId": 1,
            |  "actionId": 125
            |}
          """.stripMargin

        val expectedResponse =
          """
            |{
            |  "errors": ["action is not valid"]
            |}
          """.stripMargin

        Post("/action", parse(request)) ~>
          route() ~>
          check {
            status mustEqual StatusCodes.BadRequest
            responseAs[Json] mustEqual parse(expectedResponse).right.get
          }
      }

      "returning an error if recommendation failed" >> new TestScope {
        val expectedResponse =
          """
            |{
            |  "errors": ["video does not correspond to last given"]
            |}
          """.stripMargin

        Post("/action", parse(validRequest)) ~>
          route(failingRecommender) ~>
          check {
            status mustEqual StatusCodes.BadRequest
            responseAs[Json] mustEqual parse(expectedResponse).right.get
          }
      }
    }
  }

  trait TestScope extends Scope {
    val recommender = new TestRecommender()
    val failingRecommender = new TestRecommender(fail = true)

    def route(recommender: Recommender = recommender): Route = {
      implicit val rejectionHandler = Rejections.Handler
      seal(new HttpApi(recommender).route)
    }
  }

  class TestRecommender(fail: Boolean = false) extends Recommender {
    var lastUserInfo: UserInfo = _
    var lastUserAction: UserAction = _

    def register(user: UserInfo): Result =
      if (fail) Future.successful(UserDoesNotExist.asLeft)
      else Future.successful(Recommendation(UserId(1L), VideoId(1L)).asRight)

    def handleAction(action: UserAction): Result =
      if (fail) Future.successful(VideoDoesNotMatchLast.asLeft)
      else Future.successful(Recommendation(UserId(1L), VideoId(1L)).asRight)
  }

}
